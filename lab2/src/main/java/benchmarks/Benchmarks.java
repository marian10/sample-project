package benchmarks;

import entity.Order;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import repository.*;

import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Benchmarks {
    private static final Random random = new Random();
    private static final List<Order> orders =
            IntStream.range(0, 10000).mapToObj(i -> new Order()).collect(Collectors.toList());
    private static final Collection<Order> ordersToAdd =
            IntStream.range(0, 1000).mapToObj(i -> new Order()).collect(Collectors.toList());
    // Are different orders needed?
    private static final Collection<Order> existentOrders =
            random.ints(0, 10000).limit(1000).mapToObj(orders::get).collect(Collectors.toList());

    @State(Scope.Benchmark)
    public static class ArrayListState {
        ArrayListBasedRepository<Order> repository;

        @Setup(Level.Iteration)
        public void setup() {
            this.repository = new ArrayListBasedRepository<>();
            orders.forEach(o -> this.repository.add(o));
        }

        @TearDown(Level.Iteration)
        public void teardown() {
            System.gc();
        }
    }

    @State(Scope.Benchmark)
    public static class ConcurrentHashMapState {
        ConcurrentHashMapBasedRepository<Order> repository;

        public ConcurrentHashMapState() {
            this.repository = new ConcurrentHashMapBasedRepository<>();
            orders.forEach(o -> this.repository.add(o));
        }
    }

    @State(Scope.Benchmark)
    public static class HashSetState {
        HashSetBasedRepository<Order> repository;

        public HashSetState() {
            this.repository = new HashSetBasedRepository<>();
            orders.forEach(o -> this.repository.add(o));
        }
    }

    @State(Scope.Benchmark)
    public static class TreeSetState {
        TreeSetBasedRepository<Order> repository;

        public TreeSetState() {
            this.repository = new TreeSetBasedRepository<>();
            orders.forEach(o -> this.repository.add(o));
        }
    }

    @State(Scope.Benchmark)
    public static class EclipseSetState {
        EclipseSetBasedRepository<Order> repository;

        public EclipseSetState() {
            this.repository = new EclipseSetBasedRepository<>();
            orders.forEach(o -> this.repository.add(o));
        }
    }

    @State(Scope.Benchmark)
    public static class JutilSetState {
        JutilSetBasedRepository<Order> repository;

        public JutilSetState() {
            this.repository = new JutilSetBasedRepository<>();
            orders.forEach(o -> this.repository.add(o));
        }
    }

//    @Benchmark
    public void addArrayList(ArrayListState state) {
        for (var order : ordersToAdd)
            state.repository.add(order);
    }

    @Benchmark
    public void addConcurrentHashMap(ConcurrentHashMapState state) {
        for (var order : ordersToAdd)
            state.repository.add(order);
    }

    @Benchmark
    public void addHashSet(HashSetState state) {
        for (var order : ordersToAdd)
            state.repository.add(order);
    }

    @Benchmark
    public void addTreeSet(TreeSetState state) {
        for (var order : ordersToAdd)
            state.repository.add(order);
    }

    @Benchmark
    public void addEclipseSet(EclipseSetState state) {
        for (var order : ordersToAdd)
            state.repository.add(order);
    }

    @Benchmark
    public void addJutilSet(JutilSetState state) {
        for (var order : ordersToAdd)
            state.repository.add(order);
    }

    @Benchmark
    public void containsArrayList(ArrayListState state) {
        for (var order : existentOrders)
            state.repository.contains(order);
    }

    @Benchmark
    public void containsConcurrentHashMap(ConcurrentHashMapState state) {
        for (var order : existentOrders)
            state.repository.contains(order);
    }

    @Benchmark
    public void containsHashSet(HashSetState state) {
        for (var order : existentOrders)
            state.repository.contains(order);
    }

    @Benchmark
    public void containsTreeSet(TreeSetState state) {
        for (var order : existentOrders)
            state.repository.contains(order);
    }

    @Benchmark
    public void containsEclipseSet(EclipseSetState state) {
        for (var order : existentOrders)
            state.repository.contains(order);
    }

    @Benchmark
    public void containsJutilSet(JutilSetState state) {
        for (var order : existentOrders)
            state.repository.contains(order);
    }

    @Benchmark
    public void removeArrayList(ArrayListState state) {
        for (var order : existentOrders)
            state.repository.remove(order);
    }

    @Benchmark
    public void removeConcurrentHashMap(ConcurrentHashMapState state) {
        for (var order : existentOrders)
            state.repository.remove(order);
    }

    @Benchmark
    public void removeHashSet(HashSetState state) {
        for (var order : existentOrders)
            state.repository.remove(order);
    }

    @Benchmark
    public void removeTreeSet(TreeSetState state) {
        for (var order : existentOrders)
            state.repository.remove(order);
    }

    @Benchmark
    public void removeEclipseSet(EclipseSetState state) {
        for (var order : existentOrders)
            state.repository.remove(order);
    }

    @Benchmark
    public void removeJutilSet(JutilSetState state) {
        for (var order : existentOrders)
            state.repository.remove(order);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(Benchmarks.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
