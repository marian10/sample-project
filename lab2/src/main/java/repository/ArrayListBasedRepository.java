package repository;

import java.util.ArrayList;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T> {
    private final ArrayList<T> arrayList;

    public ArrayListBasedRepository() {
        this.arrayList = new ArrayList<>();
    }

    @Override
    public void add(T entity) {
        this.arrayList.add(entity);
    }

    @Override
    public boolean contains(T entity) {
        return this.arrayList.contains(entity);
    }

    @Override
    public void remove(T entity) {
        this.arrayList.remove(entity);
    }
}
