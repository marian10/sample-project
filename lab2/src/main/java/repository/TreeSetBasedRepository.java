package repository;

import java.util.TreeSet;

public class TreeSetBasedRepository<T extends Comparable<T>> implements InMemoryRepository<T> {
    private final TreeSet<T> treeSet;

    public TreeSetBasedRepository() {
        this.treeSet = new TreeSet<>();
    }

    @Override
    public void add(T entity) {
        this.treeSet.add(entity);
    }

    @Override
    public boolean contains(T entity) {
        return this.treeSet.contains(entity);
    }

    @Override
    public void remove(T entity) {
        this.treeSet.remove(entity);
    }
}