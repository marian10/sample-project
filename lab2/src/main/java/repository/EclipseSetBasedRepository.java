package repository;

import org.eclipse.collections.impl.set.mutable.UnifiedSet;

public class EclipseSetBasedRepository<T> implements InMemoryRepository<T> {
    private final UnifiedSet<T> unifiedSet;

    public EclipseSetBasedRepository() {
        this.unifiedSet = new UnifiedSet<>();
    }

    @Override
    public void add(T entity) {
        this.unifiedSet.add(entity);
    }

    @Override
    public boolean contains(T entity) {
        return this.unifiedSet.contains(entity);
    }

    @Override
    public void remove(T entity) {
        this.unifiedSet.remove(entity);
    }
}
