package repository;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T> {
    private final ConcurrentHashMap<T, T> concurrentHashMap;

    public ConcurrentHashMapBasedRepository() {
        this.concurrentHashMap = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T entity) {
        this.concurrentHashMap.put(entity, entity);
    }

    @Override
    public boolean contains(T entity) {
        return this.concurrentHashMap.contains(entity);
    }

    @Override
    public void remove(T entity) {
        this.concurrentHashMap.remove(entity);
    }
}
