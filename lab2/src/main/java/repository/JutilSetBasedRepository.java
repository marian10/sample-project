package repository;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;

public class JutilSetBasedRepository<T> implements InMemoryRepository<T> {
    private final ObjectOpenHashSet<T> unifiedSet;

    public JutilSetBasedRepository() {
        this.unifiedSet = new ObjectOpenHashSet<>();
    }

    @Override
    public void add(T entity) {
        this.unifiedSet.add(entity);
    }

    @Override
    public boolean contains(T entity) {
        return this.unifiedSet.contains(entity);
    }

    @Override
    public void remove(T entity) {
        this.unifiedSet.remove(entity);
    }
}
