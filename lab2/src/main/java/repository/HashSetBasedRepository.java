package repository;

import java.util.HashSet;

public class HashSetBasedRepository<T> implements InMemoryRepository<T> {
    private final HashSet<T> hashSet;

    public HashSetBasedRepository() {
        this.hashSet = new HashSet<>();
    }

    @Override
    public void add(T entity) {
        this.hashSet.add(entity);
    }

    @Override
    public boolean contains(T entity) {
        return this.hashSet.contains(entity);
    }

    @Override
    public void remove(T entity) {
        this.hashSet.remove(entity);
    }
}
