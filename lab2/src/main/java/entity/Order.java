package entity;

public class Order implements Comparable<Order> {
    private final int id;
    private final int price;
    private final int quantity;

    private static int newId = 0;

    public Order() {
        this.id = newId++;
        this.price = 0;
        this.quantity = 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        return id == order.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public int compareTo(Order o) {
        if (o == null)
            throw new NullPointerException();

        if (this == o) return 0;

        return id < o.id ? -1 : 1;
    }
}
