import logic.Service;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TestService {
    private static final List<BigDecimal> bigDecimals = IntStream
            .range(1, 11)
            .mapToObj(i -> BigDecimal.valueOf(i * 100L))
            .collect(Collectors.toList());

    @Test
    public void computeSumShouldEvaluateTo5500() {
        // Arrange
        var data = List.copyOf(bigDecimals);

        // Act
        var result = Service.computeSum(data);

        // Assert
        Assert.assertEquals(BigDecimal.valueOf(5500), result);
    }

    @Test
    public void computeAverageShouldEvaluateTo550() {
        // Arrange
        var data = List.copyOf(bigDecimals);

        // Act
        var result = Service.computeAverage(data);

        // Assert
        Assert.assertEquals(BigDecimal.valueOf(550), result);
    }
    @Test
    public void getTop10PercentShouldReturnList() {
        // Arrange
        var data = List.copyOf(bigDecimals);

        // Act
        var result = Service.getTop10Percent(data);

        // Assert
        Assert.assertEquals(result.size(), 1);
        Assert.assertEquals(result.get(0), BigDecimal.valueOf(1000));
    }
}
