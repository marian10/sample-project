import logic.Service;

import java.io.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        List<BigDecimal> bigDecimals = IntStream
                .range(1, 11)
                .mapToObj(i -> BigDecimal.valueOf(i * 10L))
                .collect(Collectors.toList());

        try {
            FileOutputStream fileOutputStream
                    = new FileOutputStream("file.dat");
            ObjectOutputStream objectOutputStream
                    = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(bigDecimals);
            objectOutputStream.flush();
            objectOutputStream.close();

            FileInputStream fileInputStream
                    = new FileInputStream("file.dat");
            ObjectInputStream objectInputStream
                    = new ObjectInputStream(fileInputStream);
            var bigDecimalStream = (List<BigDecimal>) objectInputStream.readObject();
            objectInputStream.close();

            System.out.println(Service.computeSum(bigDecimals));
            System.out.println(Service.computeAverage(bigDecimals));
            Service.getTop10Percent(bigDecimals).forEach(System.out::println);
            System.out.println("Deserialized objects:");
            bigDecimalStream.forEach(System.out::println);
        }
        catch (IOException | ClassNotFoundException exc) {
            exc.printStackTrace();
        }
    }
}
