package logic;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Service {
    public static BigDecimal computeSum(List<BigDecimal> bigDecimals) {
        return bigDecimals.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static BigDecimal computeAverage(List<BigDecimal> bigDecimals) {
        var totalWithCount = bigDecimals
                .stream()
                .map(bd -> new AbstractMap.SimpleEntry<>(bd, BigDecimal.ONE))
                .reduce((new AbstractMap.SimpleEntry<>(BigDecimal.ZERO, BigDecimal.ZERO)),
                        (pair1, pair2) -> new AbstractMap.SimpleEntry<>(pair1.getKey().add(pair2.getKey()),
                                pair1.getValue().add(pair2.getValue())));
        return totalWithCount.getKey().divide(totalWithCount.getValue(), RoundingMode.FLOOR);
    }

    public static List<BigDecimal> getTop10Percent(List<BigDecimal> bigDecimals) {
        return bigDecimals
                .stream()
                .sorted(Comparator.reverseOrder())
                .limit(bigDecimals.size() / 10)
                .collect(Collectors.toList());
    }
}
