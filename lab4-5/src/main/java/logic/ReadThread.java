package logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ReadThread extends Thread {
    private final Socket socket;
    private boolean finished;

    public ReadThread(Socket socket) {
        this.socket = socket;
        this.finished = false;
    }

    @Override
    public void run() {
        try {
            var in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String message;

            while (!finished && (message = in.readLine()) != null) {
                System.out.println(message);
            }
        }
        catch (IOException exc) {
            System.out.println("Socket read exception!");
        }
    }

    public void setFinished() {
        finished = true;
    }
}
