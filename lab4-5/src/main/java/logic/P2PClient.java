package logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class P2PClient {
    public static Map<String, Integer> clientPorts = new HashMap<>() {{
        this.put("Adam", 4564);
        this.put("Bob", 4567);
        this.put("Kyle", 4568);
    }};
    public static String hostname = "127.0.0.1";

    public static void main(String[] args) {
        try {
            var stdIn = new BufferedReader(new InputStreamReader(System.in));
            var name = getOption(stdIn, "Enter your name.", clientPorts.keySet());
            var mode = getOption(stdIn, "Input the mode\n1.Client\n2.Server",
                    Stream.of("1", "2").collect(Collectors.toSet()));

            Socket p2pSocket;

            if (mode.equals("1")) {
                var partner = getOption(stdIn, "Enter who you want to talk to.", clientPorts.keySet());

                p2pSocket = new Socket(hostname, clientPorts.get(partner));
            }
            else {
                var socket = new ServerSocket(clientPorts.get(name));
                p2pSocket = socket.accept();
                System.out.println("Someone connected!");
            }

            var out = new PrintWriter(p2pSocket.getOutputStream(), true);
            var readThread = new ReadThread(p2pSocket);
            String fromUser;
            readThread.start();

            while ((fromUser = stdIn.readLine()) != null) {
                if (fromUser.equals("Bye!")) {
                    break;
                }

                out.println(fromUser);
            }

            readThread.setFinished();
        }
        catch (IOException exc) {
            exc.printStackTrace();
        }
    }

    private static String getOption(BufferedReader stdIn, String message, Set<String> options) throws IOException {
        System.out.println(message);
        var option = stdIn.readLine().trim();

        while (!options.contains(option)) {
            System.out.println("Invalid message!");
            option = stdIn.readLine().trim();
        }

        return option;
    }
}
