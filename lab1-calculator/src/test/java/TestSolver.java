import business.Solver;
import org.junit.Assert;
import org.junit.Test;

public class TestSolver {
    @Test
    public void expressionShouldEvaluateTo15() {
        // Arrange
        var expression = "12 + 3";

        // Act
        var expectedResult = 15;
        var actualResult = Solver.evaluate(expression);

        // Assert
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void expressionShouldEvaluateTo9() {
        // Arrange
        var expression = " (2 * (3 - 4 + 5) / 3) + 7 ";

        // Act
        var expectedResult = 9;
        var actualResult = Solver.evaluate(expression);

        // Assert
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void expressionEvaluationShouldThrowIllegalArgumentException() {
        // Arrange
        var expression = "123 # 4";

        // Act & Assert
        Assert.assertThrows("The expression contains invalid characters!", IllegalArgumentException.class,
                () -> Solver.evaluate(expression));
    }

    @Test
    public void expressionEvaluationShouldThrowUnsupportedOperationException() {
        // Arrange
        var expression = "1 / 0";

        // Act & Assert
        Assert.assertThrows("Cannot divide by zero", UnsupportedOperationException.class,
                () -> Solver.evaluate(expression));
    }
}
