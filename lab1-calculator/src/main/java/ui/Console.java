package ui;

import business.Solver;

import java.util.Scanner;

public class Console {
    public void run() {
        var done = false;
        var scanner = new Scanner(System.in);

        while (!done) {
            System.out.println("Enter a valid expression to be computed or X to exit.");

            var line = scanner.nextLine();
            if (line.trim().equalsIgnoreCase("x"))
                done = true;
            else
                System.out.println(Solver.evaluate(line));
        }
    }
}
