import ui.Console;

public class Main {
    public static void main(String[] args) {
        var console = new Console();
        console.run();
    }
}
